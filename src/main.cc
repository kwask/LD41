#include "engine.hh"

int main(int argc, char const *argv[])
{
    Engine engine;

    engine.init();

    while(!engine.shouldQuit())
    {
        engine.process();
    }

    engine.deinit();
}
