#pragma once

#include <cstdlib>

#include "tile.hh"

struct World
{
    static const int WORLD_MAX_SIZE = 512;
    static const int EDGE_BUFFER = 2;

    Tile::Types tiles[WORLD_MAX_SIZE][WORLD_MAX_SIZE];

    void init()
    {
        for(int y = 0; y < WORLD_MAX_SIZE; y++)
        {
            for(int x = 0; x < WORLD_MAX_SIZE; x++)
            {
                tiles[x][y] = Tile::WALL;
            }
        }
    }

    void generate()
    {
        int room_count = 1000;
        int max_room_height = 20;
        int min_room_height = 4; 

        for(int i = 0; i < room_count; i++)
        {
            int room_height = std::rand()%max_room_height;

            if(room_height < min_room_height)
            {
                room_height = min_room_height;
            }

            int room_x = std::rand()%(WORLD_MAX_SIZE-room_height-1-EDGE_BUFFER);
            int room_y = std::rand()%(WORLD_MAX_SIZE-room_height-1-EDGE_BUFFER);

            if(room_x < EDGE_BUFFER)
            {
                room_x = EDGE_BUFFER;
            }

            if(room_y < EDGE_BUFFER)
            {
                room_y = EDGE_BUFFER;
            }

            for(int y = room_y; y < room_y+room_height; y++)
            {
                for(int x = room_x; x < room_x+room_height; x++)
                {
                    tiles[x][y] = Tile::FLOOR;
                }
            }
        }
    }
};
