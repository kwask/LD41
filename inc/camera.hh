#pragma once

struct Camera
{
    static const float MIN_ZOOM;
    static const float MAX_ZOOM;

    int x;
    int y;
    int speed;

    float zoom;
};

const float Camera::MIN_ZOOM = 1.f;
const float Camera::MAX_ZOOM = 10.f;
