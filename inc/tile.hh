#pragma once

#include <SDL2/SDL.h>

struct Tile
{
    enum Types
    {
        EMPTY,
        FLOOR,
        WALL,
        WATER,
        LAVA,
        TYPES_MAX
    };

    SDL_Color color[TYPES_MAX] = 
    {
        {  0,   0,   0, 255},
        {200, 200, 200, 255},
        {100, 100, 100, 255},
        {  0,   0, 255, 255},
        {255,   0,   0, 255}
    };

    bool solid[TYPES_MAX] = 
    {
        false,
        false,
        true,
        false,
        true
    };
};
