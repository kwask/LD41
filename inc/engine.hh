#pragma once

#include <iostream>
#include <SDL2/SDL.h>

#include "world.hh"
#include "camera.hh"

#define TILE_SIZE 32
#define RANGE_BUFFER 2

class Engine
{
private:
    bool _quit = false; // if the engine should quit

    SDL_Window* window;
    SDL_Renderer* renderer;

    World world;

    Camera cam;

    Tile tile_data;

public:
    Engine():
        cam({-256*TILE_SIZE, -256*TILE_SIZE, 30, 1.f})
    {

    }

    void init()
    {
        initGraphics();
        
        world.init();
        world.generate();
    }

    void process()
    {
        processEvents();

        clearScene();
        renderTiles();
        drawScene();
    }

    void deinit()
    {
        deinitGraphics();
    }

    void initGraphics()
    {
        if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
        {
            printf("ERROR: could not init SDL\n");
            quit();
        }

        if(!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0"))
        {
            printf("ERROR: could not set SDL hints\n");
            quit();
        }

        window = SDL_CreateWindow(
            "LD41",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            800,
            600,
            SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE
        );

        if(!window)
        {
            printf("ERROR: could not init window\n");
            quit();
        }

        renderer = SDL_CreateRenderer(
            window,
            -1,
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
        );

        if(!renderer)
        {
            printf("ERROR: could not init renderer\n");
            quit();
        }
    }

    void deinitGraphics()
    {
        if(renderer)
        {
            SDL_DestroyRenderer(renderer);
            renderer = nullptr;
        }

        if(window)
        {
            SDL_DestroyWindow(window);
            window = nullptr;
        }
    }

    void processEvents()
    {
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            handleEvent(event);
        }
    }

    void handleEvent(SDL_Event &event)
    {
        if(event.type == SDL_QUIT)
        {
            quit();
        }else if(event.type == SDL_KEYDOWN)
        { 
            switch(event.key.keysym.sym)
            {
                case SDLK_w:
                    cam.y += cam.speed;
                    break;
                case SDLK_s:
                    cam.y -= cam.speed;
                    break;
                case SDLK_a:
                    cam.x += cam.speed;
                    break;
                case SDLK_d:
                    cam.x -= cam.speed;
                    break;
                default:
                    break;
            }
        }else if(event.type == SDL_MOUSEWHEEL)
        {
            if(event.wheel.y > 0) // scroll up
            {
                cam.zoom += (Camera::MAX_ZOOM-cam.zoom)/(2*Camera::MAX_ZOOM);
            }
            else if(event.wheel.y < 0) // scroll down
            {
                cam.zoom -= (cam.zoom-Camera::MIN_ZOOM)/(2*Camera::MIN_ZOOM);
            }
        }
    }

    void clearScene()
    {
        SDL_SetRenderDrawColor(
            renderer,
            0,
            0,
            0,
            255
        );

        SDL_RenderClear(renderer);
    }

    void renderTiles()
    {
        int cam_x = (cam.x/TILE_SIZE);
        int cam_y = (cam.y/TILE_SIZE);

        int window_length;
        int window_height;

        SDL_GetWindowSize(window, &window_length, &window_height);

        int x_range = (window_length/TILE_SIZE)+RANGE_BUFFER;
        int x_offset = window_length/2;
        
        int y_range = (window_height/TILE_SIZE)+RANGE_BUFFER;
        int y_offset = window_height/2;

        for(int y = -RANGE_BUFFER; y < y_range; y++)
        {
            for(int x = -RANGE_BUFFER; x < x_range; x++)
            {
                int x_adj = (x-cam_x)-x_range/2;
                int y_adj = (y-cam_y)-y_range/2;

                if(x_adj < 0 || x_adj >= World::WORLD_MAX_SIZE)
                {
                    continue;
                }

                if(y_adj < 0 || y_adj >= World::WORLD_MAX_SIZE)
                {
                    continue;
                }

                Tile::Types type = world.tiles[x_adj][y_adj];

                SDL_SetRenderDrawColor(
                    renderer,
                    tile_data.color[type].r,
                    tile_data.color[type].g,
                    tile_data.color[type].b,
                    tile_data.color[type].a
                );
                
                int x_pos = x_offset+((x_adj*TILE_SIZE)+cam.x)*cam.zoom;
                int y_pos = y_offset+((y_adj*TILE_SIZE)+cam.y)*cam.zoom;

                int tile_length = TILE_SIZE*cam.zoom;

                SDL_Rect rect{
                    x_pos,
                    y_pos,
                    tile_length,
                    tile_length
                };

                SDL_RenderFillRect(
                    renderer,
                    &rect
                );
            }
        }
    }

    void drawScene()
    {
        SDL_RenderPresent(renderer);
    }

    void quit()
    {
        _quit = true;
    }

    bool shouldQuit()
    {
        return _quit;
    }
};
